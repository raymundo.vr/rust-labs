/* #[derive(Debug)]
enum Next<'a> {
    Nil,
    Link(LinkedList<'a>)
} */

#[derive(Debug)]
struct LinkedList<'a> {
    value: i8,
    next: &'a Option<LinkedList<'a>>,
}

trait PrintValue {
    fn print_value(&self) -> i8;
}

impl<'a> LinkedList<'a> {
    pub fn new(value: i8) -> Self {
        LinkedList {
            value,
            next: &None
        }
    }

    pub fn new_next(value: i8, next: &'a Option<LinkedList<'a>>) -> Self {
        LinkedList {
            value,
            next
        }
    }
}

impl<'a> PrintValue for LinkedList<'a> {
    fn print_value(&self) -> i8 {
        self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn simple_linked_list_works() {
        let l2 = LinkedList::new(10);
        let next_l2 = Some(l2);
        
        let l1 = LinkedList::new_next(8, &next_l2);
        let next_l1 = Some(l1);

        let root = LinkedList::new_next(6, &next_l1);

        let mut nodes = 1;
        let mut init = &root;
        while let Some(link) = init.next {
            println!("Value {}", init.print_value());
            init = link;
            nodes += 1;
        }
        println!("Last Value {}", init.print_value());

        assert_eq!(nodes, 3);
    }
}
