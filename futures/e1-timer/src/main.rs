mod timer_future;

/*
 * Our executor will work by sending tasks to run over a channel.
 * The executor will pull events off of the channel and run them.
 * When a task is ready to do more work (is awoken), it can schedule itself
 * to be polled again by putting itself back onto the channel.
 */
use {
    futures::{
        future::{BoxFuture, FutureExt},
        task::{waker_ref, ArcWake},
    },
    std::{
        future::Future,
        sync::mpsc::{sync_channel, Receiver, SyncSender},
        sync::{Arc, Mutex},
        task::{Context, Poll},
        time::Duration
    },
    timer_future::TimerFuture,
};

/// Task executor that receives tasks off of a channel and runs them.
struct Executor {
    ready_queue: Receiver<Arc<Task>>,
}

/// `Spawner` spawns new futures onto the task channel.
#[derive(Clone)]
struct Spawner {
    task_sender: SyncSender<Arc<Task>>,
}

/// A Future that can reschedule itself to be polled by an `Executor`.
struct Task {
    /// In-progress future that should be pushed to completion.
    ///
    /// The `Mutex` is not necessary for correctness, since we only have
    /// one thread executing tasks at once. However, Rust isn't smart
    /// enough to know that `future` is only mutated from one thread,
    /// so we need to use the `Mutex` to prove thread-safety. A production
    /// executor would not need this, and could use `UnsafeCell` instead.
    future: Mutex<Option<BoxFuture<'static, ()>>>,

    /// Handle to place the task itself back onto the task queue.
    task_sender: SyncSender<Arc<Task>>,
}

fn new_executor_and_spawner() -> (Executor, Spawner) {
    // Maximum number of tasks to allow queueing in the channel at once.
    // This is just to make `sync_channel` happy, and wouldn't be present in
    // a real executor.
    const MAX_Q_TASKS: usize = 10_000;
    let (task_sender, ready_queue) = sync_channel(MAX_Q_TASKS);
    (Executor { ready_queue }, Spawner { task_sender })
}

impl Spawner {
    /**
     * This method will take a future type, box it, and create a new Arc<Task>
     * with it inside which can be enqueued onto the executor.
     */
    fn spawn(&self, future: impl Future<Output = ()> + 'static + Send) {
        let future = future.boxed();
        let task = Arc::new(Task {
            future: Mutex::new(Some(future)),
            task_sender: self.task_sender.clone(),
        });

        self.task_sender.send(task).expect("Too many tasks in Q");
    }
}

// To poll futures, we'll need to create a Waker
impl ArcWake for Task {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        // Implement `wake` by re-queueing so that it will be polled again by executor.
        let cloned = arc_self.clone();
        arc_self
            .task_sender
            .send(cloned)
            .expect("too many tasks in Q");
    }
}

// Our executor then needs to pick up the task and poll it
impl Executor {
    fn run(&self) {
        while let Ok(task) = self.ready_queue.recv() {
            // Take the future and if not completed (still Some),
            // poll it in an attempt to complete it.
            let mut future_slot = task.future.lock().unwrap();
            if let Some(mut future) = future_slot.take() {
                // Create a local waker for the task itself
                let waker = waker_ref(&task);    
                let context = &mut Context::from_waker(&*waker);
                // `BoxFuture<T>` is a type alias for
                // `Pin<Box<dyn Future<Output = T> + Send + 'static>>`.
                // We can get a `Pin<&mut dyn Future + Send + 'static>`
                // from it by calling the `Pin::as_mut` method.
                if let Poll::Pending = future.as_mut().poll(context) {
                    // Put it back as it's not done yet
                    *future_slot = Some(future);
                }
            }
        }
    }
}

fn main() {
    let (executor, spawner) = new_executor_and_spawner();

    // Spawn a task
    spawner.spawn(async {
        println!("Hello, I hope you worrrk!");
        // Wait for timer
        TimerFuture::new(Duration::new(2, 0)).await;
        println!("!!Done");
    });

    // Drop the spawner so that our executor knows it's finished and won't receive
    // more incoming tasks to run
    drop(spawner);

    // Run executor until task Q is empty.
    executor.run();
}
