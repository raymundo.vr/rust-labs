/**
 * Spin a new thread when the timer is created, sleep for the required time
 * and then signal the timer Future when the time window has elapsed.
 */

use std::{
    future::Future,
    pin::Pin,
    sync::{Arc, Mutex},
    task::{Context, Poll, Waker},
    thread,
    time::Duration,
};

pub struct TimerFuture {
    shared_state: Arc<Mutex<SharedState>>,
}

/// Shared state between the future and the waiting thread
struct SharedState {
    /// Whether or not the sleep time has elapsed
    completed: bool,
    /// The waker for the task that `TimerFuture` is running on.
    /// The thread can use this after setting `completed=true` to tell
    /// `TimerFuture`'s task to wake up, see that `completed=true`, and
    /// move forward
    waker: Option<Waker>,
}

/**
 * If the thread has a shared_state.completed = true then we're Ready.
 * Otherwise clone the Waker and assign to shared_state.waker so that the
 * thread can wake the task back up.
 */
impl Future for TimerFuture {
    type Output = ();
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut shared_state = self.shared_state.lock().unwrap();
        if shared_state.completed {
            Poll::Ready(())
        } else {
            // Set waker to ensure that the thread can wake up the current task by
            // polling the future again.
            // The `TimerFuture` can move between tasks on the executor, which could
            // cause a stale waker pointing to the wrong task, preventing waking up
            // correctly. This is why is being cloned each time.
            shared_state.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}

impl TimerFuture {
    //new `TimerFuture` will complete after the provided timeout
    pub fn new(duration: Duration) -> Self {
        let shared_state = Arc::new(Mutex::new(SharedState {
            completed: false,
            waker: None
        }));

        // Spawn new thread
        let thread_shared_state = shared_state.clone();
        thread::spawn(move || {
            thread::sleep(duration);
            let mut shared_state = thread_shared_state.lock().unwrap();
            // Signal that the timer has completed and wake up the last task on which
            // the Future was polled, if any.
            shared_state.completed = true;
            if let Some(waker) = shared_state.waker.take() {
                waker.wake()
            }
        });

        TimerFuture { shared_state }
    }
}