use mini_redis::{client, Result};

//This macro is a 'block_on()' executor. An executor is necessary to run any async functionality
#[tokio::main]
pub async fn main() -> Result<()> {
    //Open connection
    let mut client = client::connect("127.0.0.1:6379").await?;

    //Create a document
    client.set("hello", "world".into()).await?;
    
    //Get doc by key
    let result = client.get("hello").await?;

    println!("Got {:?}", result);

    Ok(())
}