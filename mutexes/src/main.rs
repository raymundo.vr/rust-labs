use std::sync::{Mutex, Arc};
use std::thread;
use std::time::Duration;

/* 
// Simple case
fn main() {
    let reference = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for i in 0..10 {
        let counter = Arc::clone(&reference);
        let handler = thread::spawn(move || {
            let mut value = counter.lock().unwrap();
            println!("I got the lock: {}", i);
            thread::sleep(Duration::from_secs(1));
            *value += 1;
            println!("I modified: {}", i);
        });

        handles.push(handler);
    }

    for h in handles {
        h.join().unwrap();
    }

    println!("Result {}", *reference.lock().unwrap());
} */

fn main() {
    let reference = Arc::new(Mutex::new(vec![0, 1, 2 , 3, 4]));
    let mut handles = vec![];

    for i in 0..5 {
        let counter = Arc::clone(&reference);
        let handler = thread::spawn(move || {
            let mut value = counter.lock().unwrap();
            println!("I got the lock: {}", i);
            thread::sleep(Duration::from_secs(1));
            value[i] += 1;
            println!("I modified: {}", i);
        });

        handles.push(handler);
    }

    for h in handles {
        h.join().unwrap();
    }

    println!("Result {:?}", *reference.lock().unwrap());
}