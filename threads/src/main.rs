use std::thread;
use std::sync::mpsc;
use std::time::{ Duration, SystemTime };

fn main() {
    let (tx, rx) = mpsc::channel();
    let tx_two = tx.clone();

    thread::spawn(move || {
        let vector = vec![1,2,3,4,5,6];
        println!("Here's vector 1 {:?} {:?}", vector, SystemTime::now());
        for v in vector {
            tx.send(v).unwrap();
            thread::sleep(Duration::from_secs(2));
        }
    });

    thread::spawn(move || {
        let vector = vec![7,8,9,10,11,12];
        println!("Here's vector 2 {:?} {:?}", vector, SystemTime::now());
        for v in vector {
            thread::sleep(Duration::from_secs(1));
            tx_two.send(v).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });
    
    for received in rx {
        println!("Received {}", received);
    }
}
